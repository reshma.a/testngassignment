package org.testng;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class DataPREX {
	public static WebDriver driver;
	public static File f;
	Object[][] data=null;
	@BeforeClass
	private void Broswerlaun() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	}
	
	@DataProvider(name ="logindata")
	private Object[][] datadriven() throws BiffException, IOException {
		f= new File("/home/reshma/Documents/datadriven/testdata.xls");
		FileInputStream fi=new FileInputStream(f);
		Workbook book = Workbook.getWorkbook(fi);
		Sheet sh= book.getSheet("Sheet1");
		int rows = sh.getRows();
		int columns = sh.getColumns();
		Object fd [][]= new Object[rows-1][columns];	
		for (int i = 1; i <rows ; i++) {
			for (int j = 0; j < columns; j++) {
				fd[i-1][j]= sh.getCell(j,i).getContents();
				
				System.out.println(fd);
				
			}
		}return fd;
	}
	@Test(dataProvider = "logindata",priority = 2)
	private void exceution(String username, String password) throws InterruptedException {
		driver.get("https://demowebshop.tricentis.com/");
		driver.findElement(By.xpath("(//a[contains(text(),'Log in')])")).click();
		driver.findElement(By.xpath("(//input[@id=\"Email\"])")).sendKeys(username);
		driver.findElement(By.xpath("(//input[@id=\"Password\"])")).sendKeys(password);
		driver.findElement(By.xpath("(//input[@type=\"submit\"])[2]")).click();
		driver.findElement(By.linkText("Log out")).click();
	}
	
	
	
	
	

}
